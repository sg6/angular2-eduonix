import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GithubService{
    private username : string;
    private client_id = '58d5ac3bbc8366385cce';
    private client_secret = '424d03731c7cf0fc3f0af189b74bb055d43dbc93';

    constructor(private _http : Http) {
        console.log('Github Service ready', this.client_id);
        this.username = 'sg6';
    }

    getUser() {
        return this._http.get(`//api.github.com/users/${this.username}?client_id=${this.client_id}&client_secret=${this.client_secret}`)
            .map(res => res.json());
    }

    getRepos() {
        return this._http.get(`//api.github.com/users/${this.username}/repos?client_id=${this.client_id}&client_secret=${this.client_secret}`)
            .map(res => res.json());
    }

}