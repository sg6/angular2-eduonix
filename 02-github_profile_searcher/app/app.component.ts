import { Component } from '@angular/core';
import { GithubService } from './services/github.service';

@Component({
    selector: 'my-app',
    templateUrl: './app/components/navbar.html',
    providers : [ GithubService ]
})

export class AppComponent { }
