import { Component } from '@angular/core';
import { ServiceSpotify } from './services/spotify.service';

@Component({
  moduleId:module.id,
  selector: 'my-app',
  templateUrl: `app.component.html`,
  providers: [ServiceSpotify]
})
export class AppComponent  {  }
