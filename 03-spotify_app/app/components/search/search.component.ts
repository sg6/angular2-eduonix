import { Component } from '@angular/core';
import { ServiceSpotify } from '../../services/spotify.service';
import { Artist } from '../../../Artist';

@Component({
  moduleId:module.id,
  selector: 'search',
  templateUrl: `search.component.html`,
})
export class SearchComponent  { 

    searchStr : string;
    searchRes : Artist[];

    constructor(private _serviceSpotify : ServiceSpotify) {
        this._serviceSpotify;
    }

    searchMusic(_serviceSpotify : ServiceSpotify) {
        this._serviceSpotify.searchMusic(this.searchStr).subscribe(
            res => { 
                //console.log(res.artists.items);
                this.searchRes = res.artists.items;
            }
        );
    }


 }
