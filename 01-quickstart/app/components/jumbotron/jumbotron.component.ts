import { Component } from '@angular/core';

@Component({
    moduleId : module.id,
    selector: 'jumbotron',
    templateUrl: 'jumbotron.component.html'
})

export class JumbotronComponent {
    private headline : string;
    private par : string;
    private btn : string;
    private url : string;

    constructor() {
        this.headline = "Welcome to the Website"
        this.par = "Lorem Ipsum";
        this.btn = "Click here";
        this.url = "/about";
    }
}